import { AppDataSource } from "./data-source";
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const userRepository = AppDataSource.getRepository(User);
    const roleRepository = AppDataSource.getRepository(Role);

    const adminRole = await roleRepository.findOneBy({ id: 1 });
    const userRole = await roleRepository.findOneBy({ id: 2 });

    console.log("Inserting a new user into the database...");
    await userRepository.clear();
    var user = new User();
    user.id = 1;
    user.email = "admin";
    user.gender = "male";
    user.password = "Pass@1234";
    user.roles = [];
    user.roles.push(adminRole);
    user.roles.push(userRole);
    await userRepository.save(user);
    var findUser = await userRepository.findOneBy({ id: 1 });
    console.log(findUser);

    user = new User();
    user.id = 2;
    user.email = "user";
    user.gender = "female";
    user.password = "Pass@1234";
    user.roles = [];
    user.roles.push(userRole);
    await userRepository.save(user);
    const findUserByCondition = await userRepository.find({
      where: [{ email: "user" }, { gender: "female" }],
    });
    console.log(findUserByCondition);

    const findUsersWithRole = await userRepository.find({
      relations: { roles: true },
    });
    console.log(JSON.stringify(findUsersWithRole, null, 2));

    console.log("++++++++++++++++++++++++++++++++++++++");
    const findRoleWithUser = await roleRepository.find({
      relations: { users: true },
    });
    console.log(JSON.stringify(findRoleWithUser, null, 2));
  })

  .catch((error) => console.log(error));
