import { AppDataSource } from "./data-source";
import { Order } from "./entity/Order";
import { OrderItem } from "./entity/OrderItem";
import { Product } from "./entity/Product";
import { Role } from "./entity/Role";
import { User } from "./entity/User";

const orderDto = {
  orderItems: [
    { productId: 1, qty: 1 },
    { productId: 2, qty: 2 },
    { productId: 3, qty: 1 },
  ],
  userId: 2,
};
AppDataSource.initialize()
  .then(async () => {
    const userRepository = AppDataSource.getRepository(User);
    const productRepository = AppDataSource.getRepository(Product);
    const orderItemRepository = AppDataSource.getRepository(OrderItem);
    const orderRepository = AppDataSource.getRepository(Order);
    const user = await userRepository.findOneBy({ id: orderDto.userId });

    // new Order
    const order = new Order();
    order.user = user;
    order.total = 0;
    order.qty = 0;
    order.orderItems = [];

    // put values in to Order
    for (const io of orderDto.orderItems) {
      // new OrderItem and set values in to OrderItem
      const orderItem = new OrderItem();
      orderItem.product = await productRepository.findOneBy({
        id: io.productId,
      });
      orderItem.name = orderItem.product.name;
      orderItem.price = orderItem.product.price;
      orderItem.qty = io.qty;
      orderItem.total = orderItem.price * orderItem.qty;

      await orderItemRepository.save(orderItem);

      //set value of Order
      order.orderItems.push(orderItem);
      order.total += orderItem.total;
      order.qty += orderItem.qty;
      await orderRepository.save(order);
    }

    const order1 = await orderRepository.find({
      relations: { orderItems: true, user: true },
    });
    console.log(JSON.stringify(order1, null, 2));
  })
  .catch((error) => console.log(error));
